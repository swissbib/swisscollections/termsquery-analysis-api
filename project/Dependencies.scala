/*
 * OAI Server Interface
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import play.sbt.PlayImport.ws
import sbt._

object Dependencies {

  lazy val scalatestV = "3.1.2"



  lazy val joda = "org.joda" % "joda-convert" % "2.2.1"
  lazy val logging = "net.logstash.logback" % "logstash-logback-encoder" % "6.2"
  lazy val codingwell = "net.codingwell" %% "scala-guice" % "4.2.6"
  //"org.apache.solr" % "solr-solrj" % "7.3.1",
  //"com.fasterxml.jackson.core" % "jackson-databind" % "2.10.1",
  //"org.marc4j" % "marc4j" % "2.9.1",
  //"org.z3950.zing" % "cql-java" % "1.12",
  lazy val scalatestplusplay = "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0"

  lazy val httprequests = "com.lihaoyi" %% "requests" % "0.6.5"

  lazy val upickl = "com.lihaoyi" %% "upickle" % "1.2.0"


  lazy val solrj = "org.apache.solr" % "solr-solrj" % "7.3.1"

  lazy val gson = "com.google.code.gson" % "gson" % "2.11.0"
  lazy val solr_analysis = "org.apache.solr" % "solr-analysis-extras" % "7.3.1"

  lazy val wsDep = ws

  //lazy val memobaseServiceUtils =
  //  "org.memobase" % "memobase-service-utilities" % "2.0.9"






}
