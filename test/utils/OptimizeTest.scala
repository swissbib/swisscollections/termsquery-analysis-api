/*
 * termsquery-analysis-api
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package utils

import org.apache.solr.client.solrj.impl.HttpSolrClient

object OptimizeTest {


  //RESPONSE_LISTLENGTH=21;SOLR_COLLECTION=browsevalues-v7;SOLR_HOST=http://localhost:8081/solr;SOLR_COLLECTION_BIBLIOGRAPHIC=swisscollections-v7


  def main(args: Array[String]): Unit = {

    val client = new HttpSolrClient.Builder("http://localhost:8081/solr")
      .withConnectionTimeout(120000.toInt)
      .withSocketTimeout(120000.toInt)
      .build()

    //@param waitFlush  block until index changes are flushed to disk
    //@param waitSearcher  block until a new searcher is opened and registered as
    // the main query searcher, making the changes visible
    client.optimize("browsevalues-v8", true, true)


  }

}
