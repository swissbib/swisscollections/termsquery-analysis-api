import sbt.Keys._
import Dependencies._
//import play.sbt.PlaySettings


ThisBuild / scalaVersion := "2.13.15"
ThisBuild / organization := "ch.swisscollections"
ThisBuild / organizationName := "swisscollections"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") Some(tag)
  else None
}

lazy val root = (project in file("."))
  //Be careful : a list of things are defined in Common.scala !
  .enablePlugins(GitVersioning, PlayService, PlayLayoutPlugin, Common)
  .settings(
    name := "terms-query-analysis-api",
    //assemblyJarName in assembly := "app.jar",
    //test in assembly := {},
    //mainClass in assembly := Some("Main"),
    resolvers ++= Seq(
      //"Memobase Utils" at "https://dl.bintray.com/jonas-waeber/memobase"
      "Typesafe" at "https://repo.typesafe.com/typesafe/releases/",
      "Restlet Repository" at "https://maven.restlet.talend.com"
      //"Memobase Utils" at "https://dl.bintray.com/memoriav/memobase"
    ),
    libraryDependencies ++= Seq(
      guice,
      joda,
      codingwell,
      scalatestplusplay % Test,
      logging,
      //scala_xml_module,
      upickl,
      solrj,
      gson,
      solr_analysis,
      ws
    ),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    )

  )

PlayKeys.devSettings := Seq("play.server.http.port" -> "9000")

import com.typesafe.sbt.packager.MappingsHelper._
Universal / mappings ++= directory(baseDirectory.value / "resources")
