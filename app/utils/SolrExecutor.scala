/*
 * termsquery-analysis-api
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package utils

import com.google.gson.Gson
import modules.{AnalyzerClient, SolrJClient}
import org.apache.solr.common.params.MapSolrParams
import com.typesafe.config.Config
import ujson.Obj
import ujson.Value.Value

import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
import scala.jdk.CollectionConverters._


object SolrExecutor {

  private val entryNoGnd = 2
  private val preferredNameGnd = 3
  private val variantEntryGnd = 4


  def runQuery(endpoint: EndpointConfig, term: Option[String], isRaw: Boolean = false, backwards: Boolean = false,
               rows: Int)
              (implicit solrClient: SolrJClient,
               config: Config,

               analyzerClient: AnalyzerClient
               ): (Value, Boolean) = {

    //todo use analyzer chain from config output of first analyzer input of next
    //by now: just the first analyzer

    //todo: does this chaining of analyzers work correctly? - better naming necessary...
    // and - is this what we want?
    //val analyzedTerm = endpoint.analyzer.foldLeft(term.getOrElse("")){ case (analyzedTerm, analyzer) =>
    //  analyzerClient.analyze(analyzer,analyzedTerm)}



    val jsonResponse: (ArrayBuffer[Value], Boolean) = if (term.getOrElse("").nonEmpty && backwards) {
      readBackwards (endpoint, term.get, rows)
    }
    else {

      val analyzedTerm = analyzerClient.analyze(endpoint.analyzer.head, term.getOrElse(""))
      val map = Map("terms.limit" -> rows.toString, "terms.lower" -> analyzedTerm, "terms.sort" -> "index",
        "terms.fl" -> endpoint.solrField, "wt" -> "json", "json.nl" -> "map", "qt" -> "/terms")
      val params = new MapSolrParams(map.asJava)
      //val r: String = new Gson().toJson(solrClient.client.get.query(config.getString("solr.collection"), params))
      val r: String = new Gson().toJson(solrClient.client.get.query(solrClient.solrCollection, params))
      (ujson.read(r)("_termsResponse")("termMap")(endpoint.solrField).arr,false)

    }


    //val jsonResponse = ujson.read(r)("_termsResponse")("te  rmMap")(endpoint.solrField)
    //  .asInstanceOf[ujson.Arr]
    val uj: ArrayBuffer[Obj] = if (isRaw)  {
      jsonResponse._1.map(f =>
        ujson.Obj("fieldvalue" -> f("term"),
          "frequency" -> f("frequency")))
    }
    else {
      jsonResponse._1.value.map((f: Value) => {

        createListEntry(f)

      })
    }


    (ujson.read(uj), jsonResponse._2)
  }

  private def readBackwards (endpoint: EndpointConfig, term: String, numberOfRows:Int)
                            (implicit solrClient: SolrJClient,
                             config: Config,
                             analyzerClient: AnalyzerClient): (ArrayBuffer[Value], Boolean) = {

    //todo: make something for isRaw
    @tailrec
    def runQuery(c: Char, term: String):(ArrayBuffer[Value], Boolean) = {
      val map = Map("terms.lower" -> c.toString, "terms.sort" -> "index",
        "terms.limit" -> "-1",
        "terms.lower.incl" -> String.valueOf(true)  , "terms.upper" -> term, "terms.upper.incl" -> String.valueOf(false),
        "terms.fl" -> endpoint.solrField, "wt" -> "json", "json.nl" -> "map", "qt" -> "/terms")
      val params = new MapSolrParams(map.asJava)
      //val r = new Gson().toJson(solrClient.client.get.query(config.getString("solr.collection"), params))
      val r: String = new Gson().toJson(solrClient.client.get.query(solrClient.solrCollection, params))

      val tempResponse = ujson.read(r)("_termsResponse")("termMap")(endpoint.solrField).arr
      if (tempResponse.length > numberOfRows) {
        (tempResponse.drop(tempResponse.length - numberOfRows),true)
      } else {
        //todo: make better algorithm for previous character - tomorrow
        //we do not search before 'a'
        if ((c.toInt - 1) >= 97) {
          runQuery((c.toInt -1).toChar ,term)
        } else {
          (tempResponse, false)
        }

      }

    }
    val analyzedTerm: String = analyzerClient.analyze(endpoint.analyzer.head, term)
    runQuery(analyzedTerm.charAt(0),analyzedTerm)

  }

  val createListEntry: ujson.Value => ujson.Obj = { value =>


    val completeTermFromIndex = value("term").value.asInstanceOf[String]
    val splittedTerm = if (completeTermFromIndex.contains(s"""${"#" * 10}""")) {
      value("term").value.asInstanceOf[String].split(s"""${"#" * 10}""")
    }
    else {
      value("term").value.asInstanceOf[String].split(s"""${"x" * 10}""")
    }

    splittedTerm.length match {
      case SolrExecutor.entryNoGnd  =>
        ujson.Obj("fieldvalue" -> splittedTerm(1),
          "frequency" -> value("frequency"))
      case SolrExecutor.preferredNameGnd =>
        ujson.Obj("fieldvalue" -> splittedTerm(1),
          "frequency" -> value("frequency"),
          "references" -> splittedTerm(2))
      case SolrExecutor.variantEntryGnd =>
        ujson.Obj("fieldvalue" -> splittedTerm(1),
          "frequency" -> value("frequency"),
          "references" -> splittedTerm(2),
          "preferredName" -> splittedTerm(3))
      case _ =>
        ujson.Obj("fieldvalue" -> value("term"),
          "frequency" -> value("frequency"),
          "references" -> "data inconsistency - please inform info@swisscollections.ch")
    }
  }

}