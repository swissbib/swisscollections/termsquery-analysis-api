/*
 * termsquery-analysis-api
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package controllers

import javax.inject.Inject
import modules.{AnalyzerClient, SolrJClient}
import scala.util.{Failure, Success}
import com.typesafe.config.Config
import play.api.mvc._
import utils.SolrExecutor
import scala.util.Try
import ujson._

class TermsController @Inject()(cc: ControllerComponents,
                              ) (implicit
                                 analyzerClient: AnalyzerClient,
                                 solrClient:SolrJClient,
                                 config: Config) extends AbstractController(cc) with Rendering {


  def terms(indexField: String, term: Option[String], raw:Option[String],
            backwards: Option[String],
           rows:Option[Int]): Action[AnyContent] =
    Action { implicit request =>

      //todo:
      // - better and more tests - (in conjunction with play rules)

      val bw = Try[Boolean](backwards.get.toBoolean).getOrElse(false)
      Try[(Value, Boolean)] {


        val performedQuery: Option[(Value, Boolean)] = solrClient.endpoints.get(indexField.toLowerCase).map(SolrExecutor.runQuery(_, term,
          Try[Boolean](raw.get.toBoolean).getOrElse(false),
          bw,
          rows.getOrElse(solrClient.listlength))
        )
        if (performedQuery.isDefined) {
          performedQuery.get
        } else {
          (throw new Exception("endpoint (Solr browsefield) not available"))
        }


      }match {
          //todo specialised error type in case of special solr fields
          //no internal server error for wrong endpoint
        //case Success(None)  => InternalServerError(ujson.Obj("message" -> "endpoint (Solr browsefield) not available").toString).
        //  as("application/json")
        case Success(value) =>
          if (bw) {
            Ok(value._1.toString).as("application/json").withHeaders(("backwards", value._2.toString))
          } else {
            Ok(value._1.toString).as("application/json")
          }
        case Failure(message) => InternalServerError(ujson.Obj("message" -> message.getMessage).toString).
          as("application/json")
      }



    }



  def fallback(nonsense:Option[String]): Action[AnyContent] =
    Action { implicit request =>
      Ok(s"API fallback")
    }


}