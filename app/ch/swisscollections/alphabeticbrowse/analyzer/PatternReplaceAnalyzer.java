package ch.swisscollections.alphabeticbrowse.analyzer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordTokenizer;
import org.apache.lucene.analysis.core.KeywordTokenizerFactory;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.icu.ICUFoldingFilterFactory;
import org.apache.lucene.analysis.miscellaneous.TrimFilter;
import org.apache.lucene.analysis.miscellaneous.TrimFilterFactory;
import org.apache.lucene.analysis.pattern.PatternReplaceFilterFactory;

import java.util.HashMap;

public class PatternReplaceAnalyzer extends Analyzer {
    @Override
    protected TokenStreamComponents createComponents(String fieldName) {

        HashMap<String,String> foldingFilterMap = new HashMap<>();
        ICUFoldingFilterFactory icuFilter = new ICUFoldingFilterFactory(foldingFilterMap);

        KeywordTokenizerFactory kwtf =  new KeywordTokenizerFactory(new HashMap<>());
        KeywordTokenizer kwt = (KeywordTokenizer) kwtf.create();
        LowerCaseFilterFactory lcff = new LowerCaseFilterFactory(new HashMap<>());

        HashMap<String, String> prffHM = new HashMap<>();
        prffHM.put("pattern","\\s?&lt;&lt;.*&gt;&gt;");
        prffHM.put("replacement","");
        prffHM.put("replace","all");

        PatternReplaceFilterFactory prff = new PatternReplaceFilterFactory(prffHM);

        prffHM = new HashMap<>();
        prffHM.put("pattern","\\[|\\]|\\(|\\)|\\/|,|:|!|\\.|-|\\u0022|&amp;|&lt;|&gt;|\\u002B|&#x98;|&#x9c;|&#x92;|&#x8e;|;");
        prffHM.put("replacement"," ");
        prffHM.put("replace","all");
        PatternReplaceFilterFactory prff2 = new PatternReplaceFilterFactory(prffHM);

        prffHM = new HashMap<>();
        prffHM.put("pattern","\\s\\s*");
        prffHM.put("replacement"," ");
        prffHM.put("replace","all");
        PatternReplaceFilterFactory prff3 = new PatternReplaceFilterFactory(prffHM);

        TrimFilterFactory tfff = new TrimFilterFactory(new HashMap<>());
        TrimFilter tf =  tfff.create(prff3.create(prff2.create(prff.create(lcff.create(icuFilter.create(tfff.create(kwt)))))));

        return new TokenStreamComponents(kwt, tf);
    }
}
