package ch.swisscollections.alphabeticbrowse.analyzer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordTokenizer;
import org.apache.lucene.analysis.core.KeywordTokenizerFactory;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.icu.ICUFoldingFilterFactory;
import org.apache.lucene.analysis.miscellaneous.TrimFilter;
import org.apache.lucene.analysis.miscellaneous.TrimFilterFactory;

import java.util.HashMap;

public class BasicAnalyzer extends Analyzer {
    @Override
    protected TokenStreamComponents createComponents(String fieldName) {


        HashMap<String,String> foldingFilterMap = new HashMap<>();
        ICUFoldingFilterFactory icuFilter = new ICUFoldingFilterFactory(foldingFilterMap);


        KeywordTokenizerFactory kwtf =  new KeywordTokenizerFactory(new HashMap<>());

        KeywordTokenizer kwt = (KeywordTokenizer) kwtf.create();

        LowerCaseFilterFactory lcff = new LowerCaseFilterFactory(new HashMap<>());

        TrimFilterFactory tfff = new TrimFilterFactory(new HashMap<>());

        TrimFilter tf =  tfff.create(lcff.create(icuFilter.create(tfff.create(kwt))));

        return new TokenStreamComponents(kwt, tf);
    }
}
