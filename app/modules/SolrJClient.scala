/*
 * termsquery-analysis-api
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package modules

import com.typesafe.config.Config

import javax.inject.{Inject, Singleton}
import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.client.solrj.impl.HttpSolrClient
import play.Environment
import play.api.inject.ApplicationLifecycle
import utils.EndpointConfig

import scala.concurrent.Future
import scala.jdk.CollectionConverters._

@Singleton
class SolrJClient @Inject()(
                              lifecycle: ApplicationLifecycle,
                              val config: Config,
                              val env: Environment
                            ) extends SolrComponent {
  lifecycle.addStopHook(() => {
    //Future.successful(client.get.close())
    Future.successful(client.getOrElse(Option.empty))
  })


  override val client: Option[SolrClient] = connect()
  //println(System.getProperty("SOLR_COLLECTION"))
  //override val solrCollection: String = config.getString("SOLR_COLLECTION")
  override val solrCollection: String = getEnv("SOLR_COLLECTION")
  val endpoints: Map[String, EndpointConfig] = getEndpoints

  val listlength: Int = getEnv("RESPONSE_LISTLENGTH").toInt


  private def getEndpoints = {

    config.getConfigList("app.indexFields").asScala.map(
      c => {
        val t = c.getList( "analyzer").unwrapped.asScala.toList.map(_.asInstanceOf[String])
        c.getString( "endpoint") -> EndpointConfig(c.getString( "endpoint"),
          c.getString("solrField"),
          c.getList( "analyzer").unwrapped.asScala.toList.map(_.asInstanceOf[String])
        )
      }
    ).toMap

  }

  private def connect(): Option[SolrClient] = {
    //make it possible to work with zookeeper

    //val client = new HttpSolrClient.Builder(config.getString("solr.host"))
    val client = new HttpSolrClient.Builder(getEnv("SOLR_HOST"))
      .withConnectionTimeout(600.toInt)
      .withSocketTimeout(600.toInt)
      .build()

    Option(client)

  }

  def getEnv(value: String): String = {
    val envValue: Option[String] = sys.env.get(value)
    if (envValue.isDefined) {
      envValue.get
    } else {
      Option(System.getProperties.get(value)) match {
        case Some(value) => value.toString
        case None => throw new Exception("Environment variable " + value + " not available")
      }
    }
  }

}
