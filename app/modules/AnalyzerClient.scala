/*
 * termsquery-analysis-api
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package modules

import ch.swisscollections.alphabeticbrowse.analyzer.{BasicAnalyzer, CallNumberAnalyzer, NavFieldCombinedAnalyzer, NavFieldFormAnalyzer, PatternReplaceAnalyzer, PatternReplaceStarAnalyzer}
import com.typesafe.config.Config

import javax.inject.{Inject, Singleton}
import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import play.Environment
import play.api.inject.ApplicationLifecycle

import scala.collection.mutable.ArrayBuffer


@Singleton
class AnalyzerClient @Inject()(lifecycle: ApplicationLifecycle,
                                val config: Config,
                                val env: Environment
                              ) extends AnalyzerComponent {
  override lazy val analyzerMap: Map[String, Analyzer] = {

    Map[String, Analyzer](

      "navForm" -> new NavFieldFormAnalyzer("resources/formsynonyms.txt"),
      "navCombined" -> new NavFieldCombinedAnalyzer,
      "basic" -> new BasicAnalyzer,
      "callNumberAnalyzer" -> new CallNumberAnalyzer,
      "patternReplaceAnalyzer" -> new PatternReplaceAnalyzer,
      "patternReplaceStarAnalyzer" -> new PatternReplaceStarAnalyzer
    )
  }

    def analyze(analyzerName: String, term: String): String = {
      //todo use raw in case analyzername is not available
      val analyzer = analyzerMap(analyzerName)
      val ts = analyzer.tokenStream("fieldNotNeeded",term)

      val chars = ArrayBuffer[String]()

      ts.reset()
      while (ts.incrementToken()) {
        chars.append(ts.getAttribute(classOf[CharTermAttribute]).toString)

      }

      ts.close()
      chars.mkString

    }


}
