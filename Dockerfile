FROM sbtscala/scala-sbt:eclipse-temurin-jammy-21.0.2_13_1.10.4_2.13.15 AS build
ADD . /
WORKDIR /
RUN sbt -Dsbt.global.base=sbt-cache/sbtboot -Dsbt.boot.directory=sbt-cache/boot -Dsbt.ivy.home=sbt-cache/ivy -Dsbt.rootdir=true stage


FROM eclipse-temurin:21-jre-alpine
COPY --from=build target/universal/stage /app/

RUN apk update
RUN apk upgrade
RUN apk add bash

WORKDIR /app
ENTRYPOINT ["./bin/terms-query-analysis-api"]